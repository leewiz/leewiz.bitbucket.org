angular.module('mainApp', [])
    .controller("pulseWidthController", function($scope) {
        $scope.pulseWidthAccuracy = [".001", ".005", ".010", ".025", ".100", ".500"];
        $scope.increasePulseWidth = function(selectedAccuracy) {
            if ($scope.pulseWidth < 500) {
                $scope.pulseWidth = 500;
            } else if ($scope.pulseWidth > 2500) {
                $scope.pulseWidth = 2500;
            } else {
                switch (selectedAccuracy) {
                    case 0: $scope.pulseWidth++; break;
                    case 1: $scope.pulseWidth+=5; break;
                    case 2: $scope.pulseWidth+=10; break;
                    case 3: $scope.pulseWidth+=25; break;
                    case 4: $scope.pulseWidth+=100; break;
                    case 5: $scope.pulseWidth+=500; break;
                    default: break;
                }
                if ($scope.pulseWidth > 2500) {
                    $scope.pulseWidth = 2500;
                }
            }
        };

        $scope.decreasePulseWidth = function(selectedAccuracy) {
            if ($scope.pulseWidth < 500) {
                $scope.pulseWidth = 500;
            } else if ($scope.pulseWidth > 2500) {
                $scope.pulseWidth = 2500;
            } else {
                switch (selectedAccuracy) {
                    case 0: $scope.pulseWidth--; break;
                    case 1: $scope.pulseWidth-=5; break;
                    case 2: $scope.pulseWidth-=10; break;
                    case 3: $scope.pulseWidth-=25; break;
                    case 4: $scope.pulseWidth-=100; break;
                    case 5: $scope.pulseWidth-=500; break;
                    default: break;
                }
                if ($scope.pulseWidth < 500) {
                    $scope.pulseWidth = 500;
                }
            }
        };

    });